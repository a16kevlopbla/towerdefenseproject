﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    // Propiedades

    public float health;
    public float speed = 5f;
    public Transform rotationPart;
    public GameObject deathEffect;
    private bool slowed = false;
    private float slowTime , slowPercentage, startSpeed;
    public int money, xp;
    private WaveSpawner waveSpawner;

    private Transform target;
    private int wayPointIndex = 0;

    public Image healthBar;
    private float startHealth;
    private Canvas healthCanvas;

    private Rigidbody rbEnemy;
    private float forceTime = 2f;
    private bool dead = false;

    // Use this for initialization
    void Start()
    {
        //Reset();
        waveSpawner = FindObjectOfType<WaveSpawner>();
        startSpeed = speed;
        target = WayPoints.wayPoints[0];
        startHealth = health;
        healthCanvas = GetComponentInChildren<Canvas>();
        healthCanvas.enabled = false;
        rbEnemy = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rbEnemy.angularVelocity = Vector3.zero;
        rbEnemy.velocity = new Vector3(Mathf.Clamp(rbEnemy.velocity.x - Time.deltaTime * forceTime, 0, 20), Mathf.Clamp(rbEnemy.velocity.y - Time.deltaTime * forceTime, 0, 20), Mathf.Clamp(rbEnemy.velocity.z - Time.deltaTime * forceTime,0,20));

        if (slowed)
        {
            slowTime -= Time.deltaTime;
            if (slowTime < 0)
            {
                speed = startSpeed;
                slowed = false;
            }
        }

        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(rotationPart.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;
        rotationPart.localRotation = Quaternion.Euler(0f, rotation.y, 0f);
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);

        if (Vector3.Distance(transform.position, target.position) <= 0.2f)
        {
            nextWayPoint();
        }

    }

    void nextWayPoint()
    {
        if (wayPointIndex >= WayPoints.wayPoints.Length - 1)
        {
            waveSpawner.actualEnemies--;
            waveSpawner.enemiesRemainText.text = waveSpawner.actualEnemies + "/" + waveSpawner.totalEnemies;
            waveSpawner.playerHp--;
            Destroy(gameObject);
            return;
        }
        wayPointIndex++;
        target = WayPoints.wayPoints[wayPointIndex];
    }

    public void receiveHit(float damage)
    {
        healthCanvas.enabled = true;
        health -= damage;

        if (health > 0)
        {
            healthBar.fillAmount = Mathf.Clamp(health / startHealth, 0, 1);
        }

        if (health <= 0)
        {
            if (!dead)
            {
                dead = true;
                deathEffect = (GameObject)Instantiate(deathEffect, transform.position, deathEffect.transform.rotation);
                waveSpawner.playerMoney += money;
                waveSpawner.playerExp += xp;
                waveSpawner.actualEnemies -= 1;
                waveSpawner.enemiesRemainText.text = waveSpawner.actualEnemies + "/" + waveSpawner.totalEnemies;
                Destroy(gameObject);
            }
        }
    }

    public void slowEnemy(float percentageSlow, float slowTime)
    {
        speed = startSpeed * percentageSlow;
        this.slowTime = slowTime;
        slowed = true;
    }

    private void OnDestroy()
    {
        ParticleSystem[] particles;
        
        particles = GetComponentsInChildren<ParticleSystem>();
        if (particles != null)
        {
            foreach (ParticleSystem particle in particles)
            {
                if (particle.gameObject.tag.Contains("LightingEffect"))
                {
                    particle.gameObject.transform.parent = null;
                    particle.GetComponent<AutoDestroy>().enabled = true;
                }
            }
        }
    }

    private void Reset()
    {
        health = startHealth;
        speed = startSpeed;
    }
}
