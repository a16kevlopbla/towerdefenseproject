﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{

    public string crossbowType = "TurretCrossbow";
    public string cannonType = "TurretCannon";
    public string LightType = "TurretLight";

    public static BuildManager instance;
    public WaveSpawner waveSpawner;

    public List<GameObject> turretsPrefabs;
    public EventSystem eventSystem;

    private GameObject turretToBuild;
    private GameObject selectedTurret;
    private bool building = false, hited;
    private Touch initTouch;
    private Ray rayHit;
    private float distance;
    private GameObject currentFloor;
    private TurretController turretController;

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        // Construccion de torreta
        if (turretToBuild != null)
        {
            building = true;
            if (Input.touchCount > 0)
            {
                initTouch = Input.GetTouch(0);
                if (initTouch.phase == TouchPhase.Began)
                {
                    if (IsPointerOverUIObject(initTouch))
                    {
                        return;
                    }
                    if (eventSystem.IsPointerOverGameObject(initTouch.fingerId) || eventSystem.currentSelectedGameObject != null)
                    {
                        return;
                    }
                    rayHit = Camera.main.ScreenPointToRay(initTouch.position);
                    distance = 10000;
                    RaycastHit[] raysHits = Physics.RaycastAll(rayHit);
                    hited = false;

                    foreach (RaycastHit hit in raysHits)
                    {
                        if (hit.transform.tag.Equals("Floor"))
                        {
                            if (hit.distance < distance)
                            {
                                distance = hit.distance;
                                currentFloor = hit.transform.gameObject;
                                hited = true;
                            }
                        }
                    }
                    if (hited)
                    {
                        if (currentFloor.GetComponent<Node>().turret == null)
                        {
                            currentFloor.GetComponent<Node>().turret = (GameObject)Instantiate(turretToBuild, new Vector3(currentFloor.transform.position.x, currentFloor.transform.position.y +0.35f, currentFloor.transform.position.z), currentFloor.transform.rotation);
                            currentFloor.GetComponent<Node>().turret.GetComponent<TurretController>().floor = currentFloor.GetComponent<Node>();
                            turretController = currentFloor.GetComponent<Node>().turret.GetComponent<TurretController>();
                            turretController.totalCost += turretController.buyCost;
                            waveSpawner.playerMoney -= turretController.buyCost;
                            turretToBuild = null;
                            selectedTurret = currentFloor.GetComponent<Node>().turret;
                        }
                    }
                }
            }
        }
        else
        {
            building = false;
        }
        // Seleccionar torreta construida para mostrar rango y habilitar UI de mejora de torre
        if (!building)
        {
            if (Input.touchCount > 0)
            {
                initTouch = Input.GetTouch(0);
                if (initTouch.phase == TouchPhase.Began)
                {
                    if (IsPointerOverUIObject(initTouch))
                    {
                        return;
                    }
                    if (eventSystem.IsPointerOverGameObject(initTouch.fingerId) || eventSystem.currentSelectedGameObject != null)
                    {
                        return;
                    }
                    rayHit = Camera.main.ScreenPointToRay(initTouch.position);
                    distance = 10000;
                    RaycastHit[] raysHits = Physics.RaycastAll(rayHit);
                    hited = false;
                    GameObject turret = null;

                    foreach (RaycastHit hit in raysHits)
                    {
                        if (hit.transform.tag.Contains("Turret"))
                        {
                            if (hit.collider.Equals(hit.transform.GetComponent<TurretController>().getBoxCollider()))
                            {
                                if (hit.distance < distance)
                                {
                                    distance = hit.distance;
                                    turret = hit.transform.gameObject;
                                    hited = true;
                                }
                            }
                        }
                        /*
                         ************
                         CHEAT QUITAR
                         ************
                        */
                        if (hit.transform.tag.Equals("xpCheat"))
                        {
                            waveSpawner.playerExp += 200;
                        }
                        if (hit.transform.tag.Equals("goldCheat"))
                        {
                            waveSpawner.playerMoney += 500;
                        }
                        /*
                         ************
                         CHEAT QUITAR
                         ************
                        */
                    }
                    if (hited)
                    {
                        if (selectedTurret != null)
                        {
                            selectedTurret.GetComponent<TurretController>().rangeAttackEffect.SetActive(false);
                        }
                        turret.GetComponent<TurretController>().rangeAttackEffect.SetActive(true);
                        selectedTurret = turret;
                    }
                    else
                    {
                        if (selectedTurret != null)
                        {
                            selectedTurret.GetComponent<TurretController>().rangeAttackEffect.SetActive(false);
                            selectedTurret = null;
                        }
                    }
                }
            }
        }
        else
        {
            if (selectedTurret != null)
            {
                selectedTurret.GetComponent<TurretController>().rangeAttackEffect.SetActive(false);
            }
        }
    }

    private bool IsPointerOverUIObject(Touch touch)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(touch.position.x, touch.position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public GameObject getTurretToBuild()
    {
        return turretToBuild;
    }

    public void setTurretToBuild(GameObject turret)
    {
        turretToBuild = turret;
    }

    public GameObject getSelectedTurret()
    {
        return selectedTurret;
    }

    public void setSelectedTurret(GameObject turret)
    {
        selectedTurret = turret;
    }

    public bool isBuilding()
    {
        return building;
    }
}
