﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float camSpeed, zoomSpeed, minZoom, maxZoom, xMin, xMax, zMin, zMax;
    private Vector2 firstTouchStartPos, firstTouchActualPos, firstTouchDir, secondTouchStartPos, secondTouchActualPos, secondTouchDir;
    private Touch firstTouch, secondTouch;
    private float zoomStartDistance, zoomActualDistance;

    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touchCount == 2)
            {
                // Zoom camara
                firstTouch = Input.GetTouch(0);
                secondTouch = Input.GetTouch(1);

                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (touch.fingerId.Equals(firstTouch.fingerId))
                        {
                            firstTouchStartPos = touch.position;
                        }
                        if (touch.fingerId.Equals(secondTouch.fingerId))
                        {
                            secondTouchStartPos = touch.position;
                        }
                        if (firstTouchStartPos != null && secondTouchStartPos != null)
                        {
                            zoomStartDistance = Vector2.Distance(firstTouchStartPos, secondTouchStartPos);
                        }
                    }
                    if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
                    {
                        return;
                    }
                    if (touch.phase == TouchPhase.Stationary)
                    {
                        if (touch.fingerId.Equals(firstTouch.fingerId))
                        {
                            firstTouchStartPos = touch.position;
                        }
                        if (touch.fingerId.Equals(secondTouch.fingerId))
                        {
                            secondTouchStartPos = touch.position;
                        }
                        if (firstTouchStartPos != null && secondTouchStartPos != null)
                        {
                            zoomStartDistance = Vector2.Distance(firstTouchStartPos, secondTouchStartPos);
                        }
                    }
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (touch.fingerId.Equals(firstTouch.fingerId))
                        {
                            //firstTouchDir = firstTouchStartPos - touch.position;
                            firstTouchActualPos = touch.position;
                        }
                        if (touch.fingerId.Equals(secondTouch.fingerId))
                        {
                            //secondTouchDir = secondTouchStartPos - touch.position;
                            secondTouchActualPos = touch.position;
                        }
                        if (firstTouchActualPos != null && secondTouchActualPos != null)
                        {
                            zoomActualDistance = Vector2.Distance(firstTouchActualPos, secondTouchActualPos);
                            Vector3 cameraPos = transform.position;
                            cameraPos.y += (zoomStartDistance - zoomActualDistance) * zoomSpeed * Time.unscaledDeltaTime;
                            cameraPos.y = Mathf.Clamp(cameraPos.y, minZoom, maxZoom);
                            transform.position = cameraPos;
                        }
                    }
                }
            }
            else
            {
                // Mover camara
                firstTouch = Input.GetTouch(0);
                if (firstTouch.phase == TouchPhase.Began)
                {
                    firstTouchStartPos = firstTouch.position;
                }
                if (firstTouch.phase == TouchPhase.Ended || firstTouch.phase == TouchPhase.Canceled)
                {
                    return;
                }
                if (firstTouch.phase == TouchPhase.Stationary)
                {
                    firstTouchStartPos = firstTouch.position;
                }
                if (firstTouch.phase == TouchPhase.Moved)
                {
                    firstTouchDir = firstTouchStartPos - firstTouch.position;
                    transform.Translate(new Vector3(firstTouchDir.x, 0f, firstTouchDir.y) * camSpeed * Time.unscaledDeltaTime, Space.World);
                    Camera.main.transform.position = new Vector3(Mathf.Clamp(Camera.main.transform.position.x, xMin, xMax), Camera.main.transform.position.y, Mathf.Clamp(Camera.main.transform.position.z, zMin, zMax));
                }
            }
        }
    }
}
