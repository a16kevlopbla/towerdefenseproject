﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour {

    private BuildManager buildManager;

    public Color hoverColor;
    private Color startColor;

    public GameObject turret;
    private Renderer rend;

    void Start () {
        buildManager = BuildManager.instance;
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
    }
	
	void Update () {
        if (buildManager.isBuilding())
        {
            if (turret ==null)
            {
                rend.material.color = hoverColor;
            }
            else
            {
                rend.material.color = startColor;
            }
        }
        else
        {
            rend.material.color = startColor;
        }
	}
}
