﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class PlaySoun : MonoBehaviour
{

    private AudioSource clickAudio;
    private AudioSource backgroundAudio;
    public AudioClip backgroundClip;
    public AudioClip clickClip;
    public bool soundEffect;
    public bool soundBackground;
    public Toggle toggleQualityLow;
    public Toggle toggleQualityMedium;
    public Toggle toggleQualityHigh;
    private bool boolQualityLow = false, boolQualityMedium = false, boolQualityHigh = true;

    private Touch initTouch;

    void Start () {
        clickAudio = gameObject.AddComponent<AudioSource>();
        backgroundAudio = gameObject.AddComponent<AudioSource>();
        clickAudio.clip = clickClip;
        backgroundAudio.clip = backgroundClip;
        backgroundAudio.loop = false;
        soundEffect = true;
        soundBackground = true;
    }
	
	void Update () {
        if (soundEffect)
        {
            if (Input.touchCount > 0)
            {
                initTouch = Input.GetTouch(0);
                if (initTouch.phase == TouchPhase.Began)
                {
                    PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
                    eventDataCurrentPosition.position = new Vector2(initTouch.position.x, initTouch.position.y);
                    List<RaycastResult> results = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
                    foreach (RaycastResult result in results)
                    {
                        if (result.gameObject.layer == 9)
                        {
                            clickAudio.Play();
                            return;
                        }
                    }
                }
            }
        }
    }

    public void changeBackground()
    {
        if (backgroundAudio.loop)
        {
            backgroundAudio.loop = false;
            backgroundAudio.Stop();
        }
        else
        {
            backgroundAudio.loop = true;
            backgroundAudio.Play();
        }
    }
    public void changeEffect()
    {
        if (soundEffect)
        {
            soundEffect = false;
        }
        else
        {
            soundEffect = true;
        }
    }
    public void setQualityLow()
    {
        if (!toggleQualityLow.isOn)
        {
            boolQualityLow = false;
            boolQualityMedium = true;
            boolQualityHigh = true;
            return;
        }
        if (boolQualityLow)
        {
            boolQualityLow = false;
            QualitySettings.SetQualityLevel(0, true);
        }
    }
    public void setQualityMedium()
    {
        if (!toggleQualityMedium.isOn)
        {
            boolQualityMedium = false;
            boolQualityHigh = true;
            boolQualityLow = true;
            return;
        }
        if (boolQualityMedium)
        {
            boolQualityMedium = false;
            QualitySettings.SetQualityLevel(3, true);
        }
    }
    public void setQualityHigh()
    {
        if (!toggleQualityHigh.isOn)
        {
            boolQualityHigh = false;
            boolQualityMedium = true;
            boolQualityLow = true;
            return;
        }
        if (boolQualityHigh)
        {
            boolQualityHigh = false;
            QualitySettings.SetQualityLevel(4, true);
        }
    }
}
