﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;

public class Shop : MonoBehaviour, IPointerClickHandler
{
    private BuildManager buildManager;
    public WaveSpawner waveSpawner;

    public GameObject upgradeShop;
    private RaycastHit hit;
    private Ray originRay;
    private Touch initTouch;
    private TurretController turretController;
    private Node floor;

    [Header("Display")]
    public Text crossbowBuyText;
    public Text cannonBuyText;
    public Text lightBuyText;
    public Text sellText;
    public Text evolveCostText;
    public Text damageCostText;
    public Text rangeCostText;
    public Text attkSpdCostText;
    public Text damageTotalText;
    public Text damagePlusText;
    public Text rangeTotalText;
    public Text rangePlusText;
    public Text attkSpdTotalText;
    public Text attkSpdPlusText;
    public Text dpsText;
    public Image turretPreview;
    public Sprite crossbowIcon;
    public Sprite cannonIcon;
    public Sprite lightIcon;
    public Text speedGameText;
    public GameObject UI;
    public GameObject options;
    public GameObject enemyInfoPanel;


    public void OnPointerClick(PointerEventData eventData)
    {
        if (!buildManager.isBuilding())
        {
            if (eventData.pointerPressRaycast.gameObject.tag.Contains("enemyInfo"))
            {
                if (enemyInfoPanel.active)
                {
                    enemyInfoPanel.SetActive(false);
                }
                else
                {
                    enemyInfoPanel.SetActive(true);
                }
            }
        }
        if (!buildManager.isBuilding())
        {
            if (eventData.pointerPressRaycast.gameObject.tag.Contains("options"))
            {
                UI.SetActive(false);
                enemyInfoPanel.SetActive(false);
                options.SetActive(true);
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("back"))
            {
                options.SetActive(false);
                UI.SetActive(true);
            }
        }
        if (eventData.pointerPressRaycast.gameObject.tag.Contains("SpeedGame")){
            string speedGameString = speedGameText.text.Substring(speedGameText.text.Length - 1);
            int speedGameValue = int.Parse(speedGameString);
            if(speedGameValue == 3){
                speedGameValue = 1;
                Time.timeScale = speedGameValue;
                speedGameText.text = "Speed x" + speedGameValue;
            }else{
                speedGameValue++;
                Time.timeScale = speedGameValue;
                speedGameText.text = "Speed x" + speedGameValue;
            }
        }
        if (eventData.pointerPressRaycast.gameObject.tag.Contains("NextWave"))
        {
            waveSpawner.nextWave();
        }
        if (eventData.pointerPressRaycast.gameObject.tag.Contains("ShopIcon"))
        {
            // Buy Shop
            string turretType = "null";
            if (buildManager.getSelectedTurret() != null)
            {
                buildManager.getSelectedTurret().GetComponent<TurretController>().rangeAttackEffect.SetActive(false);
                buildManager.setSelectedTurret(null);
            }
            if (eventData.pointerPressRaycast.gameObject.tag.Contains("CrossbowShopIcon"))
            {
                turretType = "TurretCrossbow";
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("CannonShopIcon"))
            {
                turretType = "TurretCannon";
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("LightShopIcon"))
            {
                turretType = "TurretLight";
            }
            if (turretType != "null")
            {
                foreach (GameObject turret in buildManager.turretsPrefabs)
                {
                    if (turret.tag.Contains(turretType) && turret.tag.Contains("Lvl1"))
                    {
                        if (buildManager.getTurretToBuild() != null)
                        {
                            if (buildManager.getTurretToBuild().tag.Equals(turret.tag))
                            {
                                buildManager.setTurretToBuild(null);
                            }
                            else
                            {
                                if (waveSpawner.playerMoney >= turret.GetComponent<TurretController>().buyCost)
                                {
                                    buildManager.setTurretToBuild(turret);
                                }
                                else
                                {
                                    floatingTextController.CreateFloatingText("Not enough money");
                                }
                            }
                        }
                        else
                        {
                            if (waveSpawner.playerMoney >= turret.GetComponent<TurretController>().buyCost)
                            {
                                buildManager.setTurretToBuild(turret);
                            }
                            else
                            {
                                floatingTextController.CreateFloatingText("Not enough money");
                            }
                        }
                    }
                }
            }
        }
        if (buildManager.getSelectedTurret() != null)
        {
            // Upgrade Shop
            turretController = buildManager.getSelectedTurret().GetComponent<TurretController>();
            if (eventData.pointerPressRaycast.gameObject.tag.Contains("DamagePlus"))
            {
                if (turretController.damageUpgrades < 5)
                {
                    if (waveSpawner.playerMoney >= turretController.damageUpgradeCost)
                    {
                        waveSpawner.playerMoney -= turretController.damageUpgradeCost;
                        turretController.totalCost += turretController.damageUpgradeCost;
                        turretController.damage += turretController.damage * turretController.damageMultiplier;
                        turretController.damageUpgradeCost += (int)(turretController.damageUpgradeCost * (turretController.damageMultiplier * 4));
                        turretController.damageUpgrades++;
                    }
                    else
                    {
                        floatingTextController.CreateFloatingText("Not enough money");
                    }
                }
                else
                {
                    floatingTextController.CreateFloatingText("Already maxed");
                }
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("RangePlus"))
            {
                if (turretController.rangeUpgrades < 5)
                {
                    if (waveSpawner.playerMoney >= turretController.rangeUpgradeCost)
                    {
                        waveSpawner.playerMoney -= turretController.rangeUpgradeCost;
                        turretController.totalCost += turretController.rangeUpgradeCost;
                        turretController.range += turretController.range * turretController.rangeMultiplier;
                        turretController.rangeUpgradeCost += (int)(turretController.rangeUpgradeCost * (turretController.rangeMultiplier * 4));
                        turretController.rangeAttackEffect.transform.localScale = new Vector3(turretController.range * 3, turretController.range * 3, 1f);
                        turretController.rangeCollider.GetComponent<SphereCollider>().radius = turretController.range;
                        turretController.rangeUpgrades++;
                    }
                    else
                    {
                        floatingTextController.CreateFloatingText("Not enough money");
                    }
                }
                else
                {
                    floatingTextController.CreateFloatingText("Already maxed");
                }
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("AttkSpdPlus"))
            {
                if (turretController.attkSpdUpgrades < 5)
                {
                    if (waveSpawner.playerMoney >= turretController.attkSpdUpgradeCost)
                    {
                        waveSpawner.playerMoney -= turretController.attkSpdUpgradeCost;
                        turretController.totalCost += turretController.attkSpdUpgradeCost;
                        turretController.attkSpd += turretController.attkSpd * turretController.attkSpdMultiplier;
                        turretController.attkSpdUpgradeCost += (int)(turretController.attkSpdUpgradeCost * (turretController.attkSpdMultiplier * 4));
                        turretController.attkSpdUpgrades++;
                    }
                    else
                    {
                        floatingTextController.CreateFloatingText("Not enough money");
                    }
                }
                else
                {
                    floatingTextController.CreateFloatingText("Already maxed");
                }
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("Evolve"))
            {
                if (turretController.towerLvl < 4)
                {
                    if (waveSpawner.playerExp >= turretController.evolveCost)
                    {
                        waveSpawner.playerExp -= turretController.evolveCost;
                        string turretType = turretController.tag.Remove(turretController.tag.Length - 1); ;
                        int turretLvl = turretController.towerLvl;
                        turretLvl++;
                        foreach (GameObject turret in buildManager.turretsPrefabs)
                        {
                            if (turret.tag.Contains(turretType) && turret.tag.Contains(turretLvl.ToString()))
                            {
                                floor = turretController.floor;
                                GameObject evolvedTurret = (GameObject)Instantiate(turret, new Vector3(9999, 9999, 9999), floor.transform.rotation);
                                TurretController evolvedController = evolvedTurret.GetComponent<TurretController>();

                                evolvedController.totalCost = turretController.totalCost + 200;
                                if (turretController.damageUpgrades > 0)
                                {
                                    for (int i = 0; i < turretController.damageUpgrades; i++)
                                    {
                                        evolvedController.damage += evolvedController.damage * evolvedController.damageMultiplier;
                                        evolvedController.damageUpgradeCost += (int)(evolvedController.damageUpgradeCost * (evolvedController.damageMultiplier * 4));
                                        evolvedController.damageUpgrades++;
                                    }
                                }
                                if (turretController.rangeUpgrades > 0)
                                {
                                    for (int i = 0; i < turretController.rangeUpgrades; i++)
                                    {
                                        evolvedController.range += evolvedController.range * evolvedController.rangeMultiplier;
                                        evolvedController.rangeUpgradeCost += (int)(evolvedController.rangeUpgradeCost * (evolvedController.rangeMultiplier * 4));
                                        evolvedController.rangeUpgrades++;
                                    }
                                }
                                if (turretController.attkSpdUpgrades > 0)
                                {
                                    for (int i = 0; i < turretController.attkSpdUpgrades; i++)
                                    {
                                        evolvedController.totalCost += evolvedController.attkSpdUpgradeCost;
                                        evolvedController.attkSpd += evolvedController.attkSpd * evolvedController.attkSpdMultiplier;
                                        evolvedController.attkSpdUpgradeCost += (int)(evolvedController.attkSpdUpgradeCost * (evolvedController.attkSpdMultiplier * 4));
                                        evolvedController.attkSpdUpgrades++;
                                    }
                                }

                                floor.turret = evolvedTurret;
                                evolvedController.floor = floor.GetComponent<Node>();
                                evolvedController.rangeAttackEffect.transform.localScale = new Vector3(evolvedController.range * 3, evolvedController.range * 3, 1f);
                                evolvedController.rangeCollider.GetComponent<SphereCollider>().radius = evolvedController.range;
                                Destroy(turretController.gameObject);
                                evolvedTurret.transform.position = new Vector3(floor.transform.position.x, floor.transform.position.y + 0.35f, floor.transform.position.z);
                                Vector3 rangeEffectPosition = Vector3.zero;
                                rangeEffectPosition = new Vector3(evolvedTurret.transform.position.x, evolvedTurret.transform.position.y + 0.15f, evolvedTurret.transform.position.z);
                                evolvedController.rangeAttackEffect.transform.position = rangeEffectPosition;
                                turretController = evolvedController;
                                buildManager.setSelectedTurret(evolvedTurret);
                                evolvedController.rangeAttackEffect.SetActive(true);
                            }
                        }
                    }
                    else
                    {
                        floatingTextController.CreateFloatingText("Not enough exp");
                    }
                }
                else
                {
                    floatingTextController.CreateFloatingText("Already maxed");
                }
            }
            else if (eventData.pointerPressRaycast.gameObject.tag.Contains("Sell"))
            {
                waveSpawner.playerMoney += turretController.sellPrice;
                Destroy(turretController.gameObject);
            }

        }
    }

    void Start()
    {
        floatingTextController.Initialize();
        buildManager = BuildManager.instance;
        crossbowBuyText.text = "" + waveSpawner.crossbowBuyCost;
        cannonBuyText.text = "" + waveSpawner.cannonBuyCost;
        lightBuyText.text = "" + waveSpawner.lightBuyCost;
        options.SetActive(false);
        enemyInfoPanel.SetActive(false);
    }

    void Update()
    {
        if (buildManager.getSelectedTurret() == null)
        {
            upgradeShop.SetActive(false);
        }
        else
        {
            upgradeShop.SetActive(true);
            buildManager.getSelectedTurret().GetComponent<TurretController>().rangeAttackEffect.SetActive(true);
            turretController = buildManager.getSelectedTurret().GetComponent<TurretController>();
            if (buildManager.getSelectedTurret().tag.Contains("Crossbow"))
            {
                turretPreview.sprite = crossbowIcon;
            }
            else if (buildManager.getSelectedTurret().tag.Contains("Cannon"))
            {
                turretPreview.sprite = cannonIcon;
            }
            else if (buildManager.getSelectedTurret().tag.Contains("Light"))
            {
                turretPreview.sprite = lightIcon;
            }
            sellText.text = "" + turretController.sellPrice;
            damageCostText.text = "" + turretController.damageUpgradeCost.ToString("F0");
            rangeCostText.text = "" + turretController.rangeUpgradeCost.ToString("F0");
            attkSpdCostText.text = "" + turretController.attkSpdUpgradeCost.ToString("F0");
            damageTotalText.text = "" + turretController.damage.ToString("F1");
            rangeTotalText.text = "" + turretController.range.ToString("F1");
            attkSpdTotalText.text = "" + turretController.attkSpd.ToString("F1");
            dpsText.text = "" + (turretController.damage * turretController.attkSpd).ToString("F1");
            if(turretController.damageUpgrades < 5)
            {
                damagePlusText.text = "(+" + (turretController.damage * turretController.damageMultiplier).ToString("F1") + ")";
            }
            else
            {
                damagePlusText.text = "(MAX)";
                damageCostText.text = "MAX";
            }
            if (turretController.rangeUpgrades < 5)
            {
                rangePlusText.text = "(+" + (turretController.range * turretController.rangeMultiplier).ToString("F1") + ")";
            }
            else
            {
                rangePlusText.text = "(MAX)";
                rangeCostText.text = "MAX";
            }
            if (turretController.attkSpdUpgrades < 5)
            {
                attkSpdPlusText.text = "(+" + (turretController.attkSpd * turretController.attkSpdMultiplier).ToString("F1") + ")";
            }
            else
            {
                attkSpdPlusText.text = "(MAX)";
                attkSpdCostText.text = "MAX";
            }
            if (turretController.towerLvl == 4)
            {
                evolveCostText.text = "MAX";
            }
            else
            {
                evolveCostText.text = "" + turretController.evolveCost;
            }
        }
    }
}