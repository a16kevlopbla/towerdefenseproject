﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{

    [Header("Enemy prefabs from lvl 1 to 4 and earth/fire/green/water")]
    public List<Transform> enemyPrefabs;
    private List<float> enemyHealths;
    private List<float> enemyMoneyInc;
    private List<float> enemyXpInc;
    private int enemyId;
    private Transform enemy;

    [Header("Posibilities of spawn ")]
    public float stonePosibility = 0.25f;
    public float firePosibility = 0.25f;
    public float greenPosibility = 0.25f;
    public float waterPosibility = 0.25f;

    [Header("Timers and spawns")]
    public Transform spawnPoint;
    public int waveIndex = 0;
    private string enemyTypeToSpawn;
    private string[] percentageList;

    [Header("Display")]
    public Text playerMoneyText;
    public Text playerExpText;
    public Text playerHpText;
    public Text enemiesRemainText;
    public Text waveCountText;
    public GameObject nextWaveButton;
    public GameObject waveCount;
    public Text stoneHpText;
    public Text fireHpText;
    public Text greenHpText;
    public Text waterHpText;

    [Header("Prices and stats")]
    public int crossbowBuyCost;
    public int cannonBuyCost;
    public int lightBuyCost;
    public int playerMoney;
    public int playerExp;
    public int playerHp;

    [Header("Dont touch with inspector")]
    public int totalEnemies = 0;
    public int actualEnemies = 0;
    private bool waving = false;
    private int totalEnemiesSpawn;
    private float bonuxWaveExp = 25;
    private float bonuxWaveMoney = 50;
    public int totalGameEnemies = 0;

    void Start()
    {
        playerMoneyText.text = "" + playerMoney;
        playerExpText.text = "" + playerExp;
        playerHpText.text = "" + playerHp;
        enemiesRemainText.text = actualEnemies + "/" + totalEnemies;
        totalGameEnemies -= playerHp;

        waveCount.SetActive(false);
        nextWaveButton.SetActive(true);
        enemyHealths = new List<float>();
        enemyMoneyInc = new List<float>();
        enemyXpInc = new List<float>();
        for (int i = 0; i < enemyPrefabs.Count; i++)
        {
            enemyHealths.Add(enemyPrefabs[i].GetComponent<Enemy>().health);
            enemyMoneyInc.Add(enemyPrefabs[i].GetComponent<Enemy>().money);
            enemyXpInc.Add(enemyPrefabs[i].GetComponent<Enemy>().xp);
        }

        enemyInfoTextUpdate();

        int earthRounded = Mathf.RoundToInt(stonePosibility * 10);
        int fireRounded = Mathf.RoundToInt(firePosibility * 10);
        int greenRounded = Mathf.RoundToInt(greenPosibility * 10);
        int waterRounded = Mathf.RoundToInt(waterPosibility * 10);

        // Si el .5 es par se le suma 1
        if ((stonePosibility * 10) % 2 == 0.5)
        {
            earthRounded++;
        }
        if ((firePosibility * 10) % 2 == 0.5)
        {
            fireRounded++;
        }
        if ((greenPosibility * 10) % 2 == 0.5)
        {
            greenRounded++;
        }
        if ((waterPosibility * 10) % 2 == 0.5)
        {
            waterRounded++;
        }

        percentageList = new string[earthRounded + fireRounded + greenRounded + waterRounded];

        int index = 0;
        for (int i = 0; i < earthRounded; i++)
        {
            percentageList[index] = "Stone";
            index++;
        }
        for (int i = 0; i < fireRounded; i++)
        {
            percentageList[index] = "Fire";
            index++;
        }
        for (int i = 0; i < greenRounded; i++)
        {
            percentageList[index] = "Green";
            index++;
        }
        for (int i = 0; i < waterRounded; i++)
        {
            percentageList[index] = "Water";
            index++;
        }
    }

    void Update()
    {
        if(playerExp > 9999){
            playerExp = 9999;
        }
        if(playerMoney > 9999){
            playerMoney = 9999;
        }
        playerMoneyText.text = "" + playerMoney;
        playerExpText.text = "" + playerExp;
        playerHpText.text = "" + playerHp;

        if (actualEnemies <= 0)
        {
            actualEnemies = 9999;
            nextWaveButton.SetActive(true);
            waveCount.SetActive(false);
            playerMoney += (int)bonuxWaveMoney;
            playerExp += (int)bonuxWaveExp;
        }
        if (waving)
        {
            waving = false;
            waveCount.SetActive(true);
            StartCoroutine(SpawnWave());
        }
    }

    IEnumerator SpawnWave()
    {
        Debug.Log("Wave " + waveIndex);
        if (waveIndex % 10 == 5 || waveIndex % 10 == 0)
        {
            enemyTypeToSpawn = percentageList[Random.Range(0, percentageList.Length - 1)];
            Instantiate(enemyPrefabs[getEnemyToSpawn(enemyTypeToSpawn, 5)], spawnPoint.position, spawnPoint.rotation);
        }

        for (int i = 1; i <= totalEnemiesSpawn; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(0.3f);
        }
    }

    void SpawnEnemy()
    {
        enemyTypeToSpawn = percentageList[Random.Range(0, percentageList.Length - 1)];
        if (waveIndex < 5)
        {
            enemyId = getEnemyToSpawn(enemyTypeToSpawn, 1);
            enemy = (Transform) Instantiate(enemyPrefabs[enemyId], spawnPoint.position, spawnPoint.rotation);
            enemy.GetComponent<Enemy>().health = enemyHealths[enemyId];
            enemy.GetComponent<Enemy>().money = (int)enemyMoneyInc[enemyId];
            enemy.GetComponent<Enemy>().xp = (int)enemyXpInc[enemyId];
        }
        else if (waveIndex < 10)
        {
            enemyId = getEnemyToSpawn(enemyTypeToSpawn, 2);
            enemy = (Transform) Instantiate(enemyPrefabs[enemyId], spawnPoint.position, spawnPoint.rotation);
            enemy.GetComponent<Enemy>().health = enemyHealths[enemyId];
            enemy.GetComponent<Enemy>().money = (int)enemyMoneyInc[enemyId];
            enemy.GetComponent<Enemy>().xp = (int)enemyXpInc[enemyId];
        }
        else if (waveIndex < 15)
        {
            enemyId = getEnemyToSpawn(enemyTypeToSpawn, 3);
            enemy = (Transform) Instantiate(enemyPrefabs[enemyId], spawnPoint.position, spawnPoint.rotation);
            enemy.GetComponent<Enemy>().health = enemyHealths[enemyId];
            enemy.GetComponent<Enemy>().money = (int)enemyMoneyInc[enemyId];
            enemy.GetComponent<Enemy>().xp = (int)enemyXpInc[enemyId];
        }
        else
        {
            enemyId = getEnemyToSpawn(enemyTypeToSpawn, 4);
            enemy = (Transform)Instantiate(enemyPrefabs[enemyId], spawnPoint.position, spawnPoint.rotation);
            enemy.GetComponent<Enemy>().health = enemyHealths[enemyId];
            enemy.GetComponent<Enemy>().money = (int)enemyMoneyInc[enemyId];
            enemy.GetComponent<Enemy>().xp = (int)enemyXpInc[enemyId];
        }
    }

    int getEnemyToSpawn(string enemyType, int lvl)
    {
        for (int i = 0; i < enemyPrefabs.Count; i++)
        {
            if (enemyPrefabs[i].tag.Contains(enemyType) && enemyPrefabs[i].tag.Contains(lvl.ToString()))
            {
                return i;
            }
        }
        return 0;
    }

    public void nextWave()
    {
        waveIndex++;
        waveCountText.text = "Wave " + (waveIndex).ToString();

        totalEnemiesSpawn = Random.Range(waveIndex, ((waveIndex * 2) - (waveIndex / 2)) - (waveIndex / 3));
        totalEnemiesSpawn = totalEnemiesSpawn * 2;
        if(totalEnemiesSpawn > 99)
        {
            totalEnemiesSpawn = 99;
        }
        totalEnemies = totalEnemiesSpawn;
        totalGameEnemies += totalEnemiesSpawn;
        actualEnemies = totalEnemies;
        if (waveIndex % 10 == 5 || waveIndex % 10 == 0)
        {
            actualEnemies++;
            totalEnemies++;
        }
        enemiesRemainText.text = actualEnemies + "/" + totalEnemies;

        if (totalEnemiesSpawn < 99)
        {
            for (int i = 0; i < enemyHealths.Count; i++)
            {
                enemyHealths[i] = enemyHealths[i] + (enemyHealths[i] * 0.075f);
                enemyMoneyInc[i] = enemyMoneyInc[i] + (enemyMoneyInc[i] * 0.025f);
                enemyXpInc[i] = enemyXpInc[i] + (enemyXpInc[i] * 0.025f);
            }
            bonuxWaveMoney = bonuxWaveMoney + (bonuxWaveMoney * 0.05f);
            bonuxWaveExp = bonuxWaveExp + (bonuxWaveExp * 0.05f);
        }
        else
        {
            for (int i = 0; i < enemyHealths.Count; i++)
            {
                enemyHealths[i] = enemyHealths[i] + (enemyHealths[i] * 0.2f);
            }
        }

        enemyInfoTextUpdate();
        nextWaveButton.SetActive(false);
        waving = true;
    }

    public void enemyInfoTextUpdate()
    {
        if (waveIndex < 5)
        {
            for (int i = 0; i < enemyPrefabs.Count; i++)
            {
                if (enemyPrefabs[i].tag.Contains("Stone") && enemyPrefabs[i].tag.Contains("1"))
                {
                    stoneHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }else if (enemyPrefabs[i].tag.Contains("Fire") && enemyPrefabs[i].tag.Contains("1"))
                {
                    fireHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }else if (enemyPrefabs[i].tag.Contains("Green") && enemyPrefabs[i].tag.Contains("1"))
                {
                    greenHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }else if (enemyPrefabs[i].tag.Contains("Water") && enemyPrefabs[i].tag.Contains("1"))
                {
                    waterHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
            }
        }
        else if (waveIndex < 10)
        {
            for (int i = 0; i < enemyPrefabs.Count; i++)
            {
                if (enemyPrefabs[i].tag.Contains("Stone") && enemyPrefabs[i].tag.Contains("2"))
                {
                    stoneHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Fire") && enemyPrefabs[i].tag.Contains("2"))
                {
                    fireHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Green") && enemyPrefabs[i].tag.Contains("2"))
                {
                    greenHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Water") && enemyPrefabs[i].tag.Contains("2"))
                {
                    waterHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
            }
        }
        else if (waveIndex < 15)
        {
            for (int i = 0; i < enemyPrefabs.Count; i++)
            {
                if (enemyPrefabs[i].tag.Contains("Stone") && enemyPrefabs[i].tag.Contains("3"))
                {
                    stoneHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Fire") && enemyPrefabs[i].tag.Contains("3"))
                {
                    fireHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Green") && enemyPrefabs[i].tag.Contains("3"))
                {
                    greenHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Water") && enemyPrefabs[i].tag.Contains("3"))
                {
                    waterHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
            }
        }
        else
        {
            for (int i = 0; i < enemyPrefabs.Count; i++)
            {
                if (enemyPrefabs[i].tag.Contains("Stone") && enemyPrefabs[i].tag.Contains("4"))
                {
                    stoneHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Fire") && enemyPrefabs[i].tag.Contains("4"))
                {
                    fireHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Green") && enemyPrefabs[i].tag.Contains("4"))
                {
                    greenHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
                else if (enemyPrefabs[i].tag.Contains("Water") && enemyPrefabs[i].tag.Contains("4"))
                {
                    waterHpText.text = "HP: " + enemyHealths[i].ToString("F0");
                }
            }
        }
    }
}
