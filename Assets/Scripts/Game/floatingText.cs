﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class floatingText : MonoBehaviour {

    public Animator animator;
    private Text message;
    private AnimatorClipInfo[] clipInfo;

    private void OnEnable()
    {
        clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        message = animator.GetComponent<Text>();
        Destroy(gameObject, clipInfo[0].clip.length);
    }

    public void setText(string text)
    {
        message.text = text;
    }

    public void destroyNow(){
        DestroyImmediate(gameObject);
    }
}
