﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floatingTextController : MonoBehaviour {

    private static floatingText popUpText;
    private static GameObject canvas;
    private static floatingText instance;

    public static void Initialize()
    {
        canvas = GameObject.Find("CanvasInterface");
        if (!popUpText)
        {
            popUpText = Resources.Load<floatingText>("popUpTextParent");
        }
    }

    public static void CreateFloatingText(string text)
    {
        if (instance == null)
        {
            instance = Instantiate(popUpText);
            instance.transform.SetParent(canvas.transform, false);
            instance.setText(text);
        }else{
            instance.destroyNow();
            instance = Instantiate(popUpText);
            instance.transform.SetParent(canvas.transform, false);
            instance.setText(text);
        }
    }
}
