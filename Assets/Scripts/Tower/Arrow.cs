﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour
{

    private Transform target;
    private float damage;

    public float speed = 50f;
    public GameObject stoneImpactEffect;
    public GameObject greenImpactEffect;
    public GameObject waterImpactEffect;
    public GameObject fireImpactEffect;
    public GameObject explosionImpactEffect;

    // Propiedades cannon
    private float radius = 3.5f;

    public void Set(Transform target, float damage)
    {
        this.target = target;
        this.damage = damage;
    }

    // Update is called once per frame
    void Update()
    {

        // Sino existe el objetivo destruye el proyectil

        if (target == null)
        {
            Destroy(gameObject.transform.parent.gameObject);
            return;
        }

        // Calculo la direccion y la distancia que recorro este frame.
        // Si la distancia de recorrido del frame es mayor a la distancia hasta el objetivo, golpeale.

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            hitTarget();
            return;
        }

        // Mueve la distancia de este frame.

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(GetComponentInParent<Transform>());
    }

    void hitTarget()
    {
        GameObject effectInst;

        if (tag.Contains("Arrow"))
        {
            Vector3 effectPosition = Vector3.zero;
            effectPosition = new Vector3(target.position.x + 0.4f, target.position.y, target.position.z);

            if (target.tag.Contains("Stone"))
            {
                effectInst = (GameObject)Instantiate(stoneImpactEffect, effectPosition, target.rotation);
                target.GetComponent<Enemy>().receiveHit(damage);
            }
            else if (target.tag.Contains("Green"))
            {
                effectInst = (GameObject)Instantiate(greenImpactEffect, effectPosition, target.rotation);
                target.GetComponent<Enemy>().receiveHit(damage);
            }
            else if (target.tag.Contains("Water"))
            {
                effectInst = (GameObject)Instantiate(waterImpactEffect, effectPosition, target.rotation);
                target.GetComponent<Enemy>().receiveHit(damage);
            }
            else
            {
                effectInst = (GameObject)Instantiate(fireImpactEffect, effectPosition, target.rotation);
                target.GetComponent<Enemy>().receiveHit(damage);
            }

            Destroy(gameObject.transform.parent.gameObject);
            return;
        }
        else if (tag.Contains("CannonBall"))
        {
            effectInst = (GameObject)Instantiate(explosionImpactEffect, target.position, target.rotation);

            Collider[] targets = Physics.OverlapSphere(transform.position, radius);

            foreach (Collider colliderTarget in targets)
            {
                if (colliderTarget.tag.Contains("Enemy"))
                {
                    Vector3 effectPosition = Vector3.zero;
                    effectPosition = new Vector3(colliderTarget.transform.position.x + 0.4f, colliderTarget.transform.position.y, colliderTarget.transform.position.z);

                    if (colliderTarget.tag.Contains("Stone"))
                    {
                        effectInst = (GameObject)Instantiate(stoneImpactEffect, effectPosition, colliderTarget.gameObject.transform.rotation);
                        colliderTarget.gameObject.transform.GetComponent<Enemy>().receiveHit(damage);
                    }
                    else if (colliderTarget.tag.Contains("Green"))
                    {
                        effectInst = (GameObject)Instantiate(greenImpactEffect, effectPosition, colliderTarget.gameObject.transform.rotation);
                        colliderTarget.gameObject.transform.GetComponent<Enemy>().receiveHit(damage);
                    }
                    else if (colliderTarget.tag.Contains("Water"))
                    {
                        effectInst = (GameObject)Instantiate(waterImpactEffect, effectPosition, colliderTarget.gameObject.transform.rotation);
                        colliderTarget.gameObject.transform.GetComponent<Enemy>().receiveHit(damage);
                    }
                    else
                    {
                        effectInst = (GameObject)Instantiate(fireImpactEffect, effectPosition, colliderTarget.gameObject.transform.rotation);
                        colliderTarget.gameObject.transform.GetComponent<Enemy>().receiveHit(damage);
                    }
                }
            }

            Destroy(gameObject.transform.parent.gameObject);
            return;
        }
    }

    public void OnDrawGizmos()
    {
        //if (!Application.isPlaying) return;
        //Gizmos.DrawWireSphere(transform.position, radius);
    }
}
