﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{

    // VISUAL
    public List<Transform> arrowSpawns;
    public List<GameObject> arrowPrefabs;
    public GameObject rangeCollider;
    public GameObject rangeAttackEffect;
    private Transform actualTarget;
    private List<Transform> targets;
    private Collider boxCollider;

    // PROPIEDADES
    public float damage;
    public float attkSpd;
    private float fireCountdown = 0f;
    public float range;
    public int buyCost;
    public int evolveCost;
    public int damageUpgradeCost;
    public int rangeUpgradeCost;
    public int attkSpdUpgradeCost;
    public int towerLvl;

    // VISUAL
    [Header("Cannon and Arrow Towers")]
    public GameObject bodyTower;
    public GameObject headTower;
    private Quaternion originalBodyRotation;
    private Quaternion originalHeadRotation;

    [Header("Lighting Tower")]
    public int enemiesToHit = 3;
    public GameObject lightingImpactEffect = null;

    [Header("Don't touch with inspector")]
    public int damageUpgrades = 0;
    public int rangeUpgrades = 0;
    public int attkSpdUpgrades = 0;
    public int totalCost = 0;
    public int sellPrice = 0;
    public float damageMultiplier = 1f;
    public float rangeMultiplier = 1.1f;
    public float attkSpdMultiplier = 1.1f;
    public Node floor;

    private void Awake()
    {
        boxCollider = GetComponent<BoxCollider>();
        targets = new List<Transform>();
        rangeCollider.GetComponent<SphereCollider>().radius = range;
        originalBodyRotation = bodyTower.transform.localRotation;
        originalHeadRotation = headTower.transform.localRotation;

        Vector3 effectPosition = Vector3.zero;
        effectPosition = new Vector3(this.transform.position.x, this.transform.position.y + 0.15f, this.transform.position.z);
        rangeAttackEffect = (GameObject)Instantiate(rangeAttackEffect, effectPosition, this.transform.rotation);
        rangeAttackEffect.transform.localScale = new Vector3(range * 3, range * 3, 1f);
        rangeAttackEffect.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        sellPrice = totalCost / 3;
        if (actualTarget == null)
        {
            if (!this.tag.Contains("TurretLight"))
            {
                bodyTower.transform.localRotation = Quaternion.Lerp(bodyTower.transform.localRotation, originalBodyRotation, Time.deltaTime * 10f);
                headTower.transform.localRotation = Quaternion.Lerp(headTower.transform.localRotation, originalHeadRotation, Time.deltaTime * 10f);
            }
            return;
        }

        // Seguimiento torreta
        if (!this.tag.Contains("TurretLight"))
        {
            Vector3 dir = actualTarget.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotationBody = Quaternion.Lerp(bodyTower.transform.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;
            Vector3 rotationHead = Quaternion.Lerp(headTower.transform.rotation, lookRotation, Time.deltaTime * 10f).eulerAngles;
            bodyTower.transform.rotation = Quaternion.Euler(0f, rotationBody.y, 0f);
            headTower.transform.rotation = Quaternion.Euler(rotationHead.x + 2, rotationHead.y, 0f);
        }
        // Disparo y cadencia

        if (fireCountdown <= 0f)
        {
            shoot();
            fireCountdown = 1f / attkSpd;
        }

        fireCountdown -= Time.deltaTime;
    }

    void shoot()
    {
        // Instancio el proyectil y le paso el target al que seguir

        if (!tag.Contains("TurretLight"))
        {
            for (int i = 0; i < arrowSpawns.Count; i++)
            {
                GameObject arrowGO = (GameObject)Instantiate(arrowPrefabs[i], arrowSpawns[i].position, arrowSpawns[i].rotation);
                Arrow arrow = arrowGO.GetComponentInChildren<Arrow>();
                arrow.Set(actualTarget, damage / arrowSpawns.Count);
            }
        }
        else
        {
            for (int i = 0; i < enemiesToHit; i++)
            {
                if (targets.Count > i)
                {
                    if (targets[i] != null)
                    {
                        Vector3 effectPosition = Vector3.zero;
                        effectPosition = new Vector3(targets[i].position.x + 0.1f, targets[i].position.y+1f, targets[i].position.z);
                        GameObject effectInst = (GameObject)Instantiate(lightingImpactEffect, effectPosition, targets[i].rotation);
                        effectInst.transform.parent = targets[i].transform;
                        targets[i].GetComponent<Enemy>().receiveHit(damage);
                        targets[i].GetComponent<Enemy>().slowEnemy(0.5f, 3f);
                    }
                }
            }
        }
    }

    private void OnTriggerEnter(Collider obj)
    {
        if(obj!=null){
            if (obj.tag.Contains("Enemy"))
            {
                targets.Add(obj.transform);
            }
        }
    }

    private void OnTriggerStay(Collider obj)
    {
        if (obj != null)
        {
            if (actualTarget == null)
            {
                if (targets.Contains(actualTarget))
                {
                    targets.Remove(actualTarget);
                }
                if (targets.Count > 0)
                {
                    actualTarget = targets[0];
                }

            }
            else if (!targets.Contains(actualTarget))
            {
                actualTarget = null;
            }
        }
    }

    private void OnTriggerExit(Collider obj)
    {
        if (obj != null)
        {
            if (obj.tag.Contains("Enemy"))
            {
                if (actualTarget != null)
                {
                    if (actualTarget.Equals(obj.transform))
                    {
                        actualTarget = null;
                    }
                }
                targets.Remove(obj.transform);
            }
        }
    }

    public Collider getBoxCollider()
    {
        return boxCollider;
    }

    public void OnDestroy()
    {
        Destroy(rangeAttackEffect);
    }
}
