﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour, IPointerClickHandler
{

    public WaveSpawner waveSpawner;

    public GameObject gameOverPanel;
    public Text waveText;
    public Text enemiesText;
    public GameObject options;
    public GameObject enemyInfoPanel;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerPressRaycast.gameObject.tag.Contains("exit"))
        {
            Debug.Log("exit");
            Application.Quit();
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("play"))
        {
            SceneManager.LoadScene(1);
        }
    }

    void Start () {
        gameOverPanel.SetActive(false);
	}
	
	void Update () {
		if(waveSpawner.playerHp <= 0)
        {
            Shop shop = FindObjectOfType<Shop>();
            shop.UI.SetActive(false);
            options.SetActive(false);
            enemyInfoPanel.SetActive(false);
            gameOverPanel.SetActive(true);
            waveText.text = "Survived to wave: " + waveSpawner.waveIndex;
            enemiesText.text = "Enemies killed: " + waveSpawner.totalGameEnemies;
        }
	}
}
