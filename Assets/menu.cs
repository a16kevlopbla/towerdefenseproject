﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour, IPointerClickHandler
{
    public GameObject menuPanel;
    public GameObject optionsPanel;
    public GameObject guidePanel;
    public GameObject guidePanelContainer;
    public GameObject gameContainer;
    public GameObject resourcesContainer;
    public GameObject towersContainer;
    public GameObject hudContainer;
    private GameObject lastContainer;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerPressRaycast.gameObject.tag.Contains("exit"))
        {
            Application.Quit();
        }else if (eventData.pointerPressRaycast.gameObject.tag.Contains("play"))
        {
            SceneManager.LoadScene(1);
        }else if (eventData.pointerPressRaycast.gameObject.tag.Contains("options"))
        {
            optionsPanel.SetActive(true);
            menuPanel.SetActive(false);
        }else if (eventData.pointerPressRaycast.gameObject.tag.Contains("guide"))
        {
            guidePanel.SetActive(true);
            guidePanelContainer.SetActive(true);
            menuPanel.SetActive(false);
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("game"))
        {
            lastContainer.SetActive(false);
            gameContainer.SetActive(true);
            lastContainer = gameContainer;
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("resources"))
        {
            lastContainer.SetActive(false);
            resourcesContainer.SetActive(true);
            lastContainer = resourcesContainer;
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("towers"))
        {
            lastContainer.SetActive(false);
            towersContainer.SetActive(true);
            lastContainer = towersContainer;
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("hud"))
        {
            lastContainer.SetActive(false);
            hudContainer.SetActive(true);
            lastContainer = hudContainer;
        }
        else if (eventData.pointerPressRaycast.gameObject.tag.Contains("back"))
        {
            optionsPanel.SetActive(false);
            guidePanel.SetActive(false);
            guidePanelContainer.SetActive(false);
            menuPanel.SetActive(true);
        }
    }

    void Start () {
        optionsPanel.SetActive(false);
        guidePanel.SetActive(false);
        guidePanelContainer.SetActive(false);
        lastContainer = gameContainer;
    }
	
	void Update () {
		
	}
}
